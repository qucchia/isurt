<?php
try {
    include "./get.php";
    if (!$alumne) return;

    $alPati = $alumne["AlPati"];

    $alPati = $alPati ? FALSE : TRUE;

    $sql = "UPDATE Alumnes SET AlPati=" . ($alPati ? "TRUE" : "FALSE") . " $query";
    $conn->exec($sql);

    $json = array(
        "nom" => $alumne["Nom"],
        "cognom" => $alumne["Cognom"],
        "curs" => $alumne["Curs"],
        "classe" => $alumne["Classe"],
        "alPati" => $alPati
    );

    echo json_encode($json);
} catch (PDOException $e) {
    echo "Error: ", $e->getMessage();
}
?>
