function entraOSurt(e, nom, cognom, curs, classe) {
  e.target.disabled = true;
  fetch(
    "http://" +
      document.domain +
      "/entra-o-surt-del-pati/?nom=" +
      nom +
      "&cognom=" +
      cognom +
      "&curs=" +
      encodeURI(curs) +
      "&classe=" +
      classe
  )
    .then((response) => response.text())
    .then((response) => {
      document.getElementById("response").innerText = response;
      const fora = response === "L'alumne ara està a fora.";
      e.target.innerText = fora ? "Entra a classe" : "Surt al pati";
      e.target.parentElement.children[0].innerText = fora
        ? "Al pati"
        : "A dins";
      e.target.disabled = false;
    });
}

function mostraOAmaga(id) {
  const div = document.getElementById(id);
  if (div.style.visibility === "visible") {
    div.style.visibility = "hidden";
  } else {
    div.style.visibility = "visible";
  }
}
