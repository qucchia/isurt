<?php
try {
  include "./sql-login.php";

  if (!$_GET["usuari"]) {
    http_response_code(403);
    echo "Contrasenya o usuari incorrectes";
    return;
  }

  $query = "WHERE " .
    "Usuari='" . $_GET["usuari"] . "' AND " .
    "Contrasenya='" . $_GET["contrasenya"] . "'";

  $sql = "SELECT * FROM Professors $query;";
  $stmt = $conn->prepare($sql);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $result = $stmt->fetchAll();

  if (count($result) == 0) {
    http_response_code(403);
    echo "No tens permís per fer aquesta acció.";
    return;
  }

  $query = "WHERE " .
    "Nom='" . $_GET["nom"] . "' AND " .
    "Cognom='" . $_GET["cognom"] . "' AND " .
    "Curs='" . $_GET["curs"] . "' AND " .
    "Classe='" . $_GET["classe"] . "'";

  $sql = "SELECT AlPati, PotSortir FROM Alumnes $query";
  $stmt = $conn->prepare($sql);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  $result = $stmt->fetchAll();

  if (count($result) === 0) {
    http_response_code(404);
    echo "Aquest alumne no existeix.";
    return;
  }

  if (!$result[0]["AlPati"] and !$result[0]["PotSortir"]) {
    http_response_code(403);
    echo "Aquest alumne no pot sortir.";
    return;
  }

  $alPati = $result[0]["AlPati"];

  $alPati = $alPati ? FALSE : TRUE;

  $sql = "UPDATE Alumnes SET AlPati=" . ($alPati ? "TRUE" : "FALSE") . " $query";
  $conn->exec($sql);
  echo "L'alumne ara està " . ($alPati ? "a fora" : "a dins") . ".";
} catch (PDOException $e) {
  echo "Error: ", $e->getMessage();
}
