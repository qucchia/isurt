<?php
try {
    include "./sql-login.php";

    $query = "WHERE " .
        "Usuari='" . $_GET["usuari"] . "'";

    $sql = "SELECT Nom, Cognom, Curs, Classe FROM Alumnes $query";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();

    if (count($result) === 0) {
        echo "Aquest alumne/a no existeix.";
        return;
    }
    $nom = $result[0]["Nom"];
    $cognom = $result[0]["Cognom"];
    $curs = $result[0]["Curs"];
    $classe = $result[0]["Classe"];
    $dades = array("nom" => $nom, "cognom" => $cognom,
                   "curs" => $curs, "classe" => $classe);
    $qr = urlencode(json_encode($dades));
    echo "https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=$qr";
} catch (PDOException $e) {
    echo "Error: ", $e->getMessage();
}
?>
