<?php
try {
    include "./sql-login.php";

    $query = "";

    if ($_GET["usuari"]) {
        $query = "WHERE " .
            "Usuari='" . $_GET["usuari"] . "'";
    } else {
        $query = "WHERE " .
            "Nom='" . $_GET["nom"] . "' AND " .
            "Cognom='" . $_GET["cognom"] . "' AND " .
            "Curs='" . $_GET["curs"] . "' AND " .
            "Classe='" . $_GET["classe"] . "'";
    }

    $sql = "SELECT Imatge FROM Alumnes $query";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();

    if (count($result) === 0) {
        echo "Aquest alumne/a no existeix.";
        return;
    }
    header("Content-Type: image/png");
    readfile("./fotos/" . $result[0]["Imatge"]);
} catch (PDOException $e) {
    echo "Error: ", $e->getMessage();
}
?>
