<?php
try {
    include "./get.php";

    if (!$alumne) return;

    $json = array(
        "nom" => $alumne["Nom"],
        "cognom" => $alumne["Cognom"],
        "curs" => $alumne["Curs"],
        "classe" => $alumne["Classe"],
        "alPati" => $alumne["AlPati"] ? TRUE : FALSE
    );

    echo json_encode($json);
} catch (PDOException $e) {
    echo '{"error":"', $e->getMessage(), '"}';
}
?>
